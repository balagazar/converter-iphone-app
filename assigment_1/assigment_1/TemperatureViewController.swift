//
//  TemperatureViewController.swift
//  assigment_1
//
//  Created by Student on 2017-10-10.
//  Copyright © 2017 Mohawk College. All rights reserved.
//I, Alexander Logvinbenko, student number 000346587, certify that this material is my original work.
//No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.


import UIKit

class TemperatureViewController: UIViewController,UITextFieldDelegate{
    
    //Declaring Outlets
    @IBOutlet var OutPutLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    
    //declaring Temperature veriable to be a temperature measurement
    var Temperature:Measurement<UnitTemperature>?
    // declare the TypedTemperature that is currently typed
    var TypedTemperature: Double?
    
    //this method converts from Celsius to Fehrenheit
    //The method checks if the value in the text is a double and if it is it assigned the typed value and converts it to ferenheit
    //After the new temperature is stored in the new array the function creats an output converting the new temperature to string and out put in the label
    //if the values that typed are inccorect the machine will output unable to convert
    @IBAction func ConvertToFahrenheit()
    {
        
        if let text = textField.text, let value = Double(text) {
            Temperature = Measurement(value: value, unit: .celsius)
            TypedTemperature = value
            Temperature = Temperature?.converted(to: .fahrenheit)
            
            if let fahrenheitValue = Temperature , let value = TypedTemperature{
                let answerValue = numberFormatter.string(from: NSNumber(value: fahrenheitValue.value))!
                OutPutLabel.text = "\(value)℃ is \(answerValue)℉"
            } else {
                OutPutLabel.text = "Unable to Convert \(value)"
            }
        } else {
            OutPutLabel.text = "Unable to Convert \(textField.text)"
        }
        
    }
    //This method is called when the user clicks on the outside frame, so the keyboard will be released
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    //this method converts from Celsius to Fehrenheit
    //The method checks if the value in the text is a double and if it is it assigned the typed value and converts to celsius
    //After the new temperature is stored in the new array the function creats an output converting the new temperature to string and out put in the label
    //if the values that typed are inccorect the machine will output unable to convert
    @IBAction func ConvertToCelius()
    {
        if let text = textField.text, let value = Double(text) {
            Temperature = Measurement(value: value, unit: .fahrenheit)
            TypedTemperature = value
            Temperature = Temperature?.converted(to: .celsius)
   
            
            if let celsiusValue = Temperature , let value = TypedTemperature{
                let answerValue = numberFormatter.string(from: NSNumber(value: celsiusValue.value))!
                OutPutLabel.text = "\(value) ℉ is \(answerValue) ℃"
            } else {
                OutPutLabel.text = "Unable to Convert \(value)"
            }
        } else {
            OutPutLabel.text = "Unable to Convert \(textField.text)"
        }
        
    }
    //This variable creates an output with only 1 decimle when used.
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 1
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    //This method controls the textField so the user won't be able to add more periods
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")
        
        if existingTextHasDecimalSeparator != nil,
            replacementTextHasDecimalSeparator != nil {
            return false
        } else {
            return true
        }
    }

}
