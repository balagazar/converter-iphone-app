//
//  AmountViewController.swift
//  assigment_1
//
//  Created by Student on 2017-10-10.
//  Copyright © 2017 Mohawk College. All rights reserved.
//I, Alexander Logvinbenko, student number 000346587, certify that this material is my original work.
//No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.


import UIKit

class AmountViewController: UIViewController,UITextFieldDelegate{
    
    //Declaring Outlets
    @IBOutlet var OutPutLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    var Amount: Double?
    var TypedAmount: Double?
    
    //This variable creates an output with only 1 decimle when used.
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        return nf
    }()
    //This method is called when the user clicks on the outside frame, so the keyboard will be released
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    //if the textfield is double it mulptiples it in 0.81 and outs puts back as a canadian currency.
    @IBAction func ConvertToCAD()
    {
        
        if let text = textField.text, let value = Double(text) {
            Amount = value*0.81
            TypedAmount=value
            
            if let OutPutValue = Amount , let value = TypedAmount{
                let answerValue = numberFormatter.string(from: NSNumber(value: OutPutValue))!
                OutPutLabel.text = "$\(value)USD is $\(answerValue)CAD"
            } else {
                OutPutLabel.text = "Unable to convert \(value)"
            }
        }else{
            OutPutLabel.text = "Unable to convert \(textField.text)"
        }
    }
    
    //if the textfield is double it mulptiples it in 1.19 and outs puts back as a USD currency.
    //if the value is not double the method will output in the label "unable to convert"
    @IBAction func ConvertToUSD()
    {
        if let text = textField.text, let value = Double(text) {
            Amount = value*1.19
            if let OutPutValue = Amount ,let value = TypedAmount{
                let answerValue = numberFormatter.string(from: NSNumber(value: OutPutValue))!
                OutPutLabel.text = "$\(value)CAD is $\(answerValue)USD"
            } else {
                OutPutLabel.text = "Unable to convert \(value)"
            }
        }else{
            OutPutLabel.text = "Unable to convert \(textField.text)"
        }
        
    }
 
    //This method controls the textField so the user won't be able to add more periods
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")
        
        if existingTextHasDecimalSeparator != nil,
            replacementTextHasDecimalSeparator != nil {
            return false
        } else {
            return true
        }
    }
    
}
