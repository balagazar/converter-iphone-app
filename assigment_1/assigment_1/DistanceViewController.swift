//
//  DistanceViewController.swift
//  assigment_1
//
//  Created by Student on 2017-10-10.
//  Copyright © 2017 Mohawk College. All rights reserved.
//I, Alexander Logvinbenko, student number 000346587, certify that this material is my original work.
//No other person's work has been used without due acknowledgement and I have not made my work available to anyone else.

import UIKit

class DistanceViewController: UIViewController,UITextFieldDelegate{
    
    //Declaring Outlets
    @IBOutlet var OutPutLabel: UILabel!
    @IBOutlet var textField: UITextField!
    
    
    //decalres distance as a measurement unit of length
    var Distance:Measurement<UnitLength>?
    //declares the typed value as double
    var TypedDistance: Double?
    
    //If the value that is typed in textField is double it is convets to kilometers. if the input is not double the method outputs "unable to convert"
    @IBAction func ConvertToMiles()
    {
        
        if let text = textField.text, let value = Double(text) {
            Distance = Measurement(value: value, unit: .kilometers)
            TypedDistance = value
            Distance = Distance?.converted(to: .miles)
            
            if let DistanceValue = Distance, let value = TypedDistance{
                let answerValue = numberFormatter.string(from: NSNumber(value: DistanceValue.value))!
                OutPutLabel.text = "\(value)KM is \(answerValue)Miles"
            } else {
                OutPutLabel.text = "Unable to convert \(value)"
            }
            
        } else {
            OutPutLabel.text = "Unable to convert \(textField.text)"
        }
        
    }
    //This method is called when the user clicks on the outside frame, so the keyboard will be released
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    //if the value that is typed in textfield is double the method converts to to be miles and then converts the miles to kilometers. 
    //if the value is not a double the method outputs "unable to convert"
    @IBAction func ConvertToKM()
    {
        if let text = textField.text, let value = Double(text) {
            Distance = Measurement(value: value, unit: .miles)
            TypedDistance = value
            Distance = Distance?.converted(to: .kilometers)
            
            if let DistanceValue = Distance , let value = TypedDistance{
                let answerValue = numberFormatter.string(from: NSNumber(value: DistanceValue.value))!
                OutPutLabel.text = "\(value)Miles is \(answerValue)KM"
            } else {
                OutPutLabel.text = "Unable to convert \(value)"
            }
        } else {
           OutPutLabel.text = "Unable to convert \(textField.text)"
        }
        
    }
    
    //This variable creates an output with only 1 decimle when used.
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 1
        nf.maximumFractionDigits = 1
        return nf
    }()
    
    //This method controls the textField so the user won't be able to add more periods
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")
        
        if existingTextHasDecimalSeparator != nil,
            replacementTextHasDecimalSeparator != nil {
            return false
        } else {
            return true
        }
    }
    
    
}
